#-*- coding: UTF-8 -*- 
#!/usr/bin/env python
import os, sys
from utils.package import search
from utils.getUses import get_local_USE

def ScanRecoverFile():
    ScanResult = []
    if os.path.exists('./recovers.txt'):
        with open('./recovers.txt') as f:
            ScanResult = f.read().split("\n")
        res = GenRecoversFun(ScanResult)
    else:
        print("Файл со списком рекомедаций не найден.")
        res = {}
    return res

def GenRecoversFun(Reclist):
    result = {}
    category = ""
    useDis = get_local_USE()
    for r in Reclist:
        if r.startswith("["):
            category = r.replace("[", "") and r.replace("]", "")
            result[r.replace("[", "") and r.replace("]", "")] = []
        else:
            try:
                result[category].append(search(r))
                for u in search(r)['USE']:
                    if u in useDis:
                        print("u " + u +"\t"+ useDis[u]) 
            except Exception as e:
                print(e)
    return result

if  __name__ ==  '__main__':
    ScanRecoverFile()
