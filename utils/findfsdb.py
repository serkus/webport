#/usr/bin/env python 
# -*- coding: UTF-8 -*-
import os, sys
import json
from io import StringIO  
from utils.utils import get_list_overlays, sort_install_pkg, load_config
from utils.GenRecovers import ScanRecoverFile
from utils.getUses import get_global_USE, get_local_USE
from utils.package import search
from utils.parsePortage import PortageConfig
# создаём раcширенную базу пакетов с приблизительной структурой
# pkg_list ={"category": [{name: name_pkg, versions:[list_version]}]}
# и кладём это всё в json


class PortageConfig():
    def __init__(self):
        self.Config = load_config()
        self.TargetParse =['USE', 'USE_EXPAN', 'VIDEO_CARDS', 'ACCEPT_CHOSTS', 'ACCEPT_LICENSE','CCACHE_DIR', 'CCACHE_SIZE',
                    'EMERGE_DEFAULT_OPTS', 'CFLAGS', 'CXXFLAGS', 'MAKEOPTS']
        self.MakeConfig={}
        self.EnvPortaga ={}
        #super().__init__(self.run())

    def getLines(self, filename):
        if os.path.exists(filename):
            with open (filename, 'r') as f:
                lines = f.read().split("\n")
        else:
            print("path is not Found")
        return lines

    def parseMakeConf(self):
        StateBox = 'Null'
        Lines = self.getLines(self.Config['Portage']['make.conf'])

        for line in Lines:
            if not line.startswith('#') and len(line.split("="))>1:
                line.replace('"', "")
                if StateBox == 'Null' and not line.endswith('"'):
                    StateBox = line.split('=')[0]
                    self.MakeConfig[StateBox] =[] 
                                    
                    self.MakeConfig[StateBox] = line.split('=')[1].split(" ")
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[split('=')[0]].append(i)
                elif StateBox !='Null' and not line.endswith('"'):
                    StateBox = line.split('=')[1]
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[line.split('=')[0]].append(i)
                elif StateBox == 'Null' and line.endswith('"'):
                    #line.replace('"', "")
                    self.MakeConfig[line.split('=')[0]] = line.split('="')[1].split(" ")
        self.EnvPortaga['make.conf'] = self.MakeConfig        
        print(self.MakeConfig)
    
    def templParse(self, parm, res=False):
        self.MakeConfig[parm] =[]
        Lines = self.getLines(self.Config['Portage'][parm])
        for i in Lines:
            if not i.startswith('#'):
                self.MakeConfig[parm].append(dict(Name=i.split(" ")[0], Uses=i.split(" ")[1:]))
            
        if res:
            print(self.MakeConfig[parm])
            return self.MakeConfig[parm]
            
        self.EnvPortaga[pam] = self.MakeConfig[parm]

    def parsePackageUse(self):
      self.templParse('package.use')

    def parsePackageMasks(self):
        sel.templParse('package.mask')

    def parsePakageUnmask(self):
        self.templParse("package.unmask")

    def parsePackageKeywords(self):
        self.templParse('package.accept_keywords')

    def run(self):
       self.parseMakeConf()
       self.templParse('package.use', res=True)
       self.templParse('package.mask', res=True)
       self.templParse('package.unmask', res=True)
       self.templParse('package.accept_keywords', res=True)
       return self.EnvPortaga


if __name__ ==  '__main__':
    Test = PortageConfig()
    if sys.argv[1] == "envMake":   
        Test.parseMakeConf()
    elif sys.argv[1] == "use":
        Test.templParse('package.use', res=True)
    elif sys.argv[1] == "mask":
        Test.templParse('package.mask', res=True)
    elif sys.argv[1] == "unmask":
        Test.templParse('package.unmask', res=True)
    else:
        pass


#

AliaseCategory ={
				"app-crypt":"Шифрование",
    			"app-dicts":"Словари",
    			"app-editors":"Текстовые редакторы",
				"www-client":"Браузеры",
				"www-servers": "Веб-серверы",
				'app-etitor':"Текстовые редакторы",
				"game":"Игры",
				"dev-lang":" Языкы программирования",
				"media-video": "Видео",
				"media-sound": "Аудио",
				"dev-erlang":"Erlang",
				"dev-java":"Java",
				"dev-perl":"Perl",
				"dev-python":"Python",
				"net-voip":"VoIP",
				"media-fonts":"Шрифты",
				"media-gfx":"Графика",
				"media-radio":"Радио",
				"media-tv":"ТВ",
				"net-vpn":"VPN",
				"sci-chemistry":"Химия",
				"sci-electronics":"Электроника",
				"sci-biology":"Биология",
				"sci-astronomy":"Астрономия",
				"sci-geosciences":"География",
				"sci-mathematics":"Математика",
				"sci-physics":"Физика",
				"app-office":"Офис",
				"mail-client":"Клиент e-mail",
				"net-dns":"DNS-сервера",
				"sys-apps":"Системное",
				"sys-kernel":"Ядра",
				"11-themes":"Темы",
				"app-emulation":"Виртуализация",
				"x11-drivers":"Видеодрайвера",
				"app-antivirus":"Антивирус",
				"games-fps":"Шутеры",
				"games-kids":"Детские игры",
				"games-rpg":"RPG Игры",
				"games-puzzle":"Головоломки",
				"games-strategy":"Стратегии",
				 }

def create_db():
	#overlays = get_list_overlays()
	#recovers = ScanRecoverFile()
	pkg_list = {}
	listUses = dict(LocalUSE = get_local_USE(), GlobalUSE = get_global_USE()) #[0] LocalUSE = get_local_USE(),
	#InstallPkgs = sort_install_pkg()
	#portage_list = {}
	#PortEnv = PortageConfig().run()
	port_dir = ["/var/db/repos", "/usr/portage"]
	if not os.path.exists('./pkgs.json'):
		print("Create ramdb")
		pkg_name = ""
		all_pkgs = {}
		with open('./pkgs.json', 'a') as fn:
			for p in port_dir:
				for d, dirs, files in os.walk(p):
					for f in files:
						if f.endswith('.ebuild'):
							#print(d +"/" + f)
							pkg_name = ""
							try:
								#ver = int(f.replace('.ebuild', '').split('-')[-1][0])
								for pn in f.replace('.ebuild', '').split('-')[:-1]:
									pkg_name = pkg_name + pn + "-"
							except TypeError:
								for pn in f.replace('.ebuild', '').split('-')[:-2]:
									pkg_name = pkg_name + pn + "-"
							except Exception as e:
								print(e)

							if not pkg_name[:-1] in all_pkgs:
								#portage_list[pkg_name] = search(pkg_name)
								#print

								#print(str(d.split("/")[-2] +"/" + d.split("/")[-1] +"\n"))
								if str(d.split("/")[-2] +"/" + d.split("/")[-1]) in AliaseCategory:
									all_pkgs.append(AliaseCategory[d])
								else:
									all_pkgs[str(d.split("/")[-2] +"/" + d.split("/")[-1])] = str(d.split("/")[-2] +"/" + d.split("/")[-1])
									#portage_list[str(d.split("/")[-2] +"/" + d.split("/")[-1])] = search(str(d.split("/")[-2] +"/" + d.split("/")[-1]))

							if d.split("/")[-2] not in pkg_list.keys():
								pkg_list[str(d.split("/")[-2])] = [] 
								pkg_list[d.split("/")[-2]].append(d.split('/')[-1])
								#if search(str(d.split("/")[-2]))['Name'] != 'Package is not Found':
									#portage_list[str(d.split("/")[-2] +"/" + d.split("/")[-1])] = search(str(d.split("/")[-2]))
								#	print([d.split("/")[-2]])
							else:
								if d.split('/')[-1] not in pkg_list[d.split('/')[-2]]:
									pkg_list[d.split("/")[-2]].append(d.split('/')[-1])
									#print([d.split('/')[-1]])
			#"all_pkgs": all_pkgs, [0]
			InstallPkgs = sort_install_pkg(all_pkgs)
            
			fn.write(json.dumps({ "all_pkgs": all_pkgs, "Catalog": pkg_list, "overlays": get_list_overlays(),  "aliases": AliaseCategory, "recovers": ScanRecoverFile(), "usesDecription":listUses, "InstallPkgs":sort_install_pkg(), 'EnvPortage': PortageConfig().run()}))
		print(len(pkg_list))
	"""
	with open('./portage.json', 'w') as p:
		p.write(json.dumps({'portage': portage_list}))
		#print("Found:\t"+ str(len(pkg_list.keys())) +  " category\n" + str(len(all_pkgs)) +"packages\n")
	"""
def on_find(p_v):
	if not os.path.exists('./pkgs.json'):
		create_db()
	p = []
	ret_p = ""
	ret = {}
	with open('./pkgs.json', 'r') as  fn:
		data = fn.read()
		#pkg_list = fn.read()
		
		pkg_list = data.split("\n")
		for i in pkg_list:
			if p_v in i and not i in p:
				print(i)
				p.append(str(i))
				#ret_p = ret_p +"\t" + i
	print("Find in template:\t" + str(len(p)))
	
	#ret = {"Name": ret_p.split("\t")}
	#print(ret_p)	
	#print(p)
	return pkg_list #json.dumps(ret)

if __name__ == '__main__':
	if not os.path.exists('./pkgs.txt'):
		create_db()
	if len(sys.argv) >= 2:
		on_find(sys.argv[1])
	else:
		print("No element to find")

