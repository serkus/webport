#-*- codding:UTF-8 -*-
import os, sys, json
try:
    from utils.utils import load_config
except Exception as  e:
    print(e)

class PortageConfig():
    def __init__(self):
        self.Config = load_config()
        self.TargetParse =['USE', 'USE_EXPAN', 'VIDEO_CARDS', 'ACCEPT_CHOSTS', 'ACCEPT_LICENSE','CCACHE_DIR', 'CCACHE_SIZE',
                    'EMERGE_DEFAULT_OPTS', 'CFLAGS', 'CXXFLAGS', 'MAKEOPTS']
        
        self.MakeConfig={}
        self.EnvPortaga ={}
        super().__init__(self.run())

    def getLines(self, filename):
        if os.path.exists(filename):
            with open (filename, 'r') as f:
                lines = f.read().split("\n")
        else:
            print("path is not Found")
        return lines

    def parseMakeConf(self):
        StateBox = 'Null'
        Lines = self.getLines(self.Config['Portage']['make.conf'])

        for line in Lines:
            if not line.startswith('#') and len(line.split("="))>1:
                line.replace('"', "")
                if StateBox == 'Null' and not line.endswith('"'):
                    StateBox = line.split('=')[0]
                    self.MakeConfig[StateBox] =[] 
                                    
                    self.MakeConfig[StateBox] = line.split('=')[1].split(" ")
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[split('=')[0]].append(i)
                elif StateBox !='Null' and not line.endswith('"'):
                    StateBox = line.split('=')[1]
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[line.split('=')[0]].append(i)
                elif StateBox == 'Null' and line.endswith('"'):
                    #line.replace('"', "")
                    self.MakeConfig[line.split('=')[0]] = line.split('="')[1].split(" ")
        self.EnvPortaga['make.conf'] = self.MakeConfig       
        print(self.MakeConfig)
    
    def templParse(self, parm, res=False):
        self.MakeConfig[parm] =[]
        Lines = self.getLines(self.Config['Portage'][parm])
        for i in Lines:
            self.MakeConfig[parm].append(dict(Name=i.split(" ")[0], Uses=i.split(" ")[1:]))
        
        if res:
            print(self.MakeConfig[parm])
            return self.MakeConfig[parm]
        
        self.EnvPortaga[pam] = self.MakeConfig[parm]

    def parsePackageUse(self):
      self.templParse('package.use')

    def parsePackageMasks(self):
        sel.templParse('package.mask')

    def parsePakageUnmask(self):
        self.templParse("package.unmask")

    def parsePackageKeywords(self):
        self.templParse('package.accept_keywords')

    def run(self):
       self.parseMakeConf()
       self.templParse('package.use', res=True)
       self.templParse('package.mask', res=True)
       self.templParse('package.unmask', res=True)
       self.templParse('package.accept_keywords', res=True)


if __name__ ==  '__main__':
    Test = PortageConfig()
    if sys.argv[1] == "envMake":   
        Test.parseMakeConf()
    elif sys.argv[1] == "use":
        Test.templParse('package.use', res=True)
    elif sys.argv[1] == "mask":
        Test.templParse('package.mask', res=True)
    elif sys.argv[1] == "unmask":
        Test.templParse('package.unmask', res=True)
    else:
        pass
