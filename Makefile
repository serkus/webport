#!/bin/bash

run:
	#make cert &&
	python server.py 

testToken:
	python core/jwt.py

cert:
	openssl req -new -x509 -keyout localhost.pem -out localhost.pem -nodes -days 365

testEnv:
	python  utils/parsePortage.py envMake

testEnvUse:
	python  utils/parsePortage.py use

testEnvMask:
	python  utils/parsePortage.py mask

testEnvUnMask:
	python  utils/parsePortage.py unmask
	
test:
	echo 'test'




